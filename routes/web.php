

<!-- // Route::get('/', function () {
//     return view('welcome');
// }); -->


<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Faker\Factory;

class Topic{}


  // Route::get('/about', function () {
  //     return view('about');
  // });

  Route::get('/', function () {
    $faker=Factory::create();


    $topic1=new Topic();
    $topic1->image = $faker->imageUrl($width = 600, $height =300);

    $topic1->mdate = $faker->monthName($max = 'now');
    $topic1->ddate = $faker->dayOfMonth($max = 'now');
    $topic1->ydate = $faker->year($max = 'now');
    $topic1->name = $faker->name;
    $topic1->title = $faker->text(40);
    $topic1->text=$faker->text($maxNbChars = 400);


    $topic2=new Topic();

    $topic2->image = $faker->imageUrl($width = 600, $height = 300);
    $topic2->title=$faker->text(50);
    $topic2->mdate = $faker->monthName($max = 'now');
    $topic2->ddate = $faker->dayOfMonth($max = 'now');
    $topic2->ydate = $faker->year($max = 'now');
    $topic2->name = $faker->name;
    $topic2->text=$faker->text($maxNbChars = 300);


    $topic3=new Topic();

    $topic3->image = $faker->imageUrl($width = 600, $height = 300);
    $topic3->title = $faker->text(30);
    $topic3->mdate = $faker->monthName($max = 'now');
    $topic3->ddate = $faker->dayOfMonth($max = 'now');
    $topic3->ydate = $faker->year($max = 'now');
    $topic3->name = $faker->name;
    $topic3->text=$faker->text($maxNbChars = 350);


    $data=[
      'topics'=>[$topic1,$topic2,$topic3],
      'banner'=>$faker->imageUrl($width=200, $height=180, 'people')//"https://www.google.ca/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"
    ];

    return view('welcome',$data);
  });









  /*<?php

  /*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
  */

  // Route::get('/', function () {
  // return view('welcome');
  // });
