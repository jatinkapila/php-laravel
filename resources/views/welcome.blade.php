@extends('layout')

@section('content')


  <header class="logo">
    <h1 class="site-title">
      <a href="#">The Classy Simple Life</a></h1>
      {{-- <div class="slide-image">
        <a href="#" rel="bookmark">
            <img src='' /> </a>
      </div> --}}
        </header>

    <hr>
    <div class="hero">
      <div class="thirty">
        <a href="#">ABOUT</a>
        <a href="#">LIFE</a>
        <a href="#">STYLE</a>
        <a href="#">WELLNESS</a>
        <a href="#">TIPS</a>
        <a href="#">CONTACT</a>
      </div>
    </div>
    <hr>
    <div class="box">

       <figure>
       <img src="https://i1.wp.com/www.theclassysimplelife.com/wp-content/uploads/2017/11/Michelle-Sidebar-Alice-and-Olivia.jpg?resize=250%2C375&amp;ssl=1" alt="123px" width="250px" height="375" alt="test" >
        </figure>

        <div class="text">
         <center> <strong>HI, I’M MICHELLE !!</strong></center>
          <div style="width:300px; height:40px;">


          <p style="text-align: center;">Welcome to The Classy Simple Life. A minimalist lifestyle blog sharing simple living tips, travel, minimalist fashion, interiors and more. Come join the journey to meaningful living.</p>


         <center> <h6 style="text-align: center;"><a href="https://www.theclassysimplelife.com/about/">READ MORE</a></h6></center>

        </div>

     </div>
</div>





    <div class="container">

          <?php foreach ($topics as $topic): ?>

            <h1><a href="#"><?php echo $topic->title ?></a></h1>
            <br/>
            <a href="#"><?php echo $topic->mdate ?>
              <?php echo $topic->ddate ?>,
              <?php echo $topic->ydate ?> ~

              <a href="#"><?php echo ''."READ THE POST" ?></a>
              <a href="#"><?php echo ''."Leave a Comment" ?></a>
              <br/>
              <br/>
              <a href="#"><img src="<?php echo $topic->image ?>" alt=""></a>
              <br/>
              <br/>
              <?php echo $topic->text ?>
              <br/>
              <br/>
              <a href="#">Continue reading →</a>
              <hr>
            <?php endforeach; ?>

          </div>

          <div class="links">
            <h4>GET INSPIRED</h4>
            <!-- <h3>Sign up for the FREE Newsletter</h3><br/> -->
            <p>Get access to updates, exclusive content, and more!</p>

            <br/>
            <form>
              <label for="subbox1" class="screenread">First Name</label><br/><br/>
              <input type="text" id="subbox1" class="enews-subbox" value="" placeholder="First Name" name="first_name"><br/><br/>
              <input type="text" name="email" value="Enter Email Address"><br/><br/>
              <input type="submit" value="SUBSCRIBE" id="subbutton">
            </form>
            <p>P.S. No spam. Ever.</p>
            <br/>
            <hr>
              </div>
            <div class="post">


            <h3>Recent Posts</h3>
            <a href="#"><?php echo "MY 2018 READING LIST" ?></a>
            <time itemprop="datePublished" datetime="2018-02-05T09:00:58+00:00">March 5, 2018</time>
            By
            <span><a href="#">2 Comments</a></span>
            <hr><br /><br />
            <a href="#"><?php echo "10 Free Resources to Build A Successful Blog."?></a>
            <time class="entry-time" itemprop="datePublished" datetime="2018-02-05T09:00:58+00:00">February 5, 2018</time>
            By
            <span><a href="#">2 Comments</a></span>
            <hr><br /><br />
            <a href="#"><?php echo "Love Yourself First: Minimalism and Self-Love"?></a>
            <hr><br /><br />
            <a href="#"><?php echo "4 Simple Ways to Be Less Addicted to Your Smartphone"?></a>
            <time itemprop="datePublished" datetime="2018-02-05T09:00:58+00:00">January 30, 2018</time>
            By
            <span><a href="#">7 Comments</a></span>
            <hr><br /><br />
            <a href="#"><?php echo "Why I Stopped Budgeting &amp; What I Do Now"?></a>
          </div>


        <footer>

          <a href="#"><?php echo "OLDER POSTS"?></a>
          <h4 style="text-align:center;">@MICHSUMMERFIELD</h4>
        </footer>
        <br/>
        <p style="text-align:center;">Copyright ©&nbsp;2018 · <a href="#">Simply Pro Theme</a> On <a href="#">Genesis Framework</a> ·
          <a href="#">WordPress</a> · <a rel="#" href="#">Log in</a></p>

        @endsection
